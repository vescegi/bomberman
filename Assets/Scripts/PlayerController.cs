using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private SmoothMove _smoothMove;
    private Grid grid;
    public Vector2Int GridPosition { get; private set; }

    private void Awake()
    {
        _smoothMove = GetComponent<SmoothMove>();
    }

    public void Initialize(Grid grid, Vector2Int gridPosition)
    {
        this.grid = grid;
        this.GridPosition = gridPosition;
    }

    private void Update()
    {
        Move();
        PlaceBomb();
    }

    private void Move()
    {
        if (!_smoothMove.InPosition) return;

        Vector2Int direction;
        
        float horizontalAxis = Input.GetAxisRaw("Horizontal");
        float verticalAxis = Input.GetAxisRaw("Vertical");


        // Get input
        if (Mathf.Abs(horizontalAxis) == 1f)
        {
            direction = new Vector2Int((int)horizontalAxis, 0);
        }
        else if (Mathf.Abs(verticalAxis) == 1f)
        {
            direction = new Vector2Int(0, (int)verticalAxis);
        }
        else
            return;

        Vector2Int possibleNewPosition = GridPosition + direction;

        if(grid.IsAvailable(possibleNewPosition.x, possibleNewPosition.y))
        {
            Vector2Int oldPosition = GridPosition;
            GridPosition = possibleNewPosition;

            if (!grid.IsBomb(oldPosition.x, oldPosition.y))
            {
                grid.SwapCell(oldPosition, GridPosition);
            } 
            else
            {
                /* 
                 * Because the player spawns the bomb beneath his feets
                 * the cell wich the player is on can't be both a bomb and a player cell,
                 * so, when a bomb is spawned, the cell which the player sits on is now a bomb cell
                 * and the next cell the player moves will become a player cell
                 */

                grid.SetPlayerCell(GridPosition.x, GridPosition.y);
            }

            _smoothMove.Move(new Vector3(direction.x, direction.y));
        }

    }

    private void PlaceBomb()
    {
        if(Input.GetButtonDown("Jump"))
        {
            grid.PlaceBomb(GridPosition.x, GridPosition.y);
        }
    }
}
