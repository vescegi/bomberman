using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothMove : MonoBehaviour
{
    [SerializeField] float moveSpeed;

    [SerializeField] private Transform movePoint;

    public bool InPosition { get; private set; }

    private void Awake()
    {
        movePoint.parent = transform.parent;
        //movePoint.position = transform.position;
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, movePoint.position, moveSpeed * Time.deltaTime);
        
        InPosition = Vector3.Distance(transform.position, movePoint.position) < 0.1f;
    }

    public void Move(Vector3 delta)
    {
        movePoint.transform.position += delta;
    }
}
