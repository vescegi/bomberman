using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float moveDelay;

    private Grid grid;

    private Vector2Int gridPosition;

    private SmoothMove _smoothMove;

    public void Initialize(Grid grid, Vector2Int gridPosition)
    {
        this.grid = grid;
        this.gridPosition = gridPosition;
    }

    private void Awake()
    {
        _smoothMove = GetComponent<SmoothMove>();
    }

    private void Start()
    {
        StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        while (true)
        {
            Vector2Int[] availableDirections = grid.AvailableDirections(gridPosition.x, gridPosition.y);

            if (availableDirections.Length != 0)
            {
                Vector2Int randomDirection = availableDirections[Random.Range(0, availableDirections.Length)];

                Vector2Int oldPosition = gridPosition;
                gridPosition += randomDirection;

                if (grid.IsPlayer(gridPosition.x, gridPosition.y))
                    grid.GameOver();

                grid.SwapCell(oldPosition, gridPosition);

                Vector3 direction = new Vector3(randomDirection.x, randomDirection.y);
                _smoothMove.Move(direction);
            }

            yield return new WaitForSeconds(moveDelay);
        }
    }

    
}
