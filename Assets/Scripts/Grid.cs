using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

#region Utility
public enum CellType
{
    None = 0,
    Brick = 2,
    Block = 4,
    Enemy = 8,
    Player = 16,
    Bomb = 32
}
public struct Cell
{
    public CellType type;
    public GameObject gameObject;

    public Cell(CellType type, GameObject gameObject)
    {
        this.type = type;
        this.gameObject = gameObject;
    }
}
public class CellUnavailableException : Exception
{
    public CellUnavailableException()
    {
    }

    public CellUnavailableException(string message)
        : base(message)
    {
    }

    public CellUnavailableException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
#endregion

public class Grid : MonoBehaviour
{
    [SerializeField] int size;
    [SerializeField, Range(0f, 1f)] float bricksSpawnRate;
    [SerializeField] int enemiesToSpawn;

    [Header("References")]
    [SerializeField] GameObject brickPrefab;
    [SerializeField] GameObject blockPrefab;
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] GameObject playerPrefab;
    [SerializeField] GameObject bombObject;

    private Cell[,] grid;
    private readonly Cell emptyCell = new Cell(CellType.None, null);
    private Cell bombCell;
    private Cell playerCell;

    private Bomb bomb;
    private PlayerController player;


    private int explodedEnemies;
    private void Awake()
    {
        grid = new Cell[size, size];
        bombCell = new Cell(CellType.Bomb, bombObject);

        explodedEnemies = 0;

        GenerateMap();
        GenerateEnemies();
        GeneratePlayer();
    }

    private void Start()
    {
        bomb = bombObject.GetComponent<Bomb>();
        bomb.Initialize(this);
    }

    #region Generation
    private GameObject SpawnObject(int x, int y, GameObject prefab, CellType type)
    {
        GameObject objectSpawned = Instantiate(prefab, new Vector3Int(x - size / 2, y - size / 2), Quaternion.identity, transform);
        objectSpawned.name += x + " " + y;

        grid[x, y] = new Cell(type, objectSpawned);

        return objectSpawned;
    }

    #region Map Generation
    private void GenerateMap()
    {
        
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                grid[i, j] = emptyCell;

                if (CanBlockSpawn(i, j))
                {
                    SpawnObject(i, j, blockPrefab, CellType.Block);
                }
                else
                {
                    SpawnBrick(i, j);
                }
            }
        }
    }

    private bool CanBlockSpawn(int x, int y)
    {
        return x == 0 || y == 0 ||                  // Top/Left borders
               x == size - 1 || y == size - 1 ||    // Bottom/Right borders
               (y % 2 == 0 && x % 2 == 0);          // Grid pattern
    }

    private void SpawnBrick(int x, int y)
    {
        if (PlayerSafeZone(x, y)) return;
        if (Random.value > bricksSpawnRate) return;

        SpawnObject(x, y, brickPrefab, CellType.Brick);
    }

    #endregion

    private void GenerateEnemies()
    {
        for (int i = 0; i < enemiesToSpawn; i++)
        {

            bool placed;
            do
            {
                int x = Random.Range(0, size);
                int y = Random.Range(0, size);

                if (IsAvailable(x, y) && !PlayerSafeZone(x, y))
                {
                    SpawnObject(x, y, enemyPrefab, CellType.Enemy).GetComponent<Enemy>().Initialize(this, new Vector2Int(x, y));
                    placed = true;
                }
                else
                {
                    placed = false;
                }
            } while (!placed);
        }
    }

    private void GeneratePlayer()
    {
        int x = 1;
        int y = size - 2;

        player = SpawnObject(x, y, playerPrefab, CellType.Player).GetComponent<PlayerController>();
        playerCell = grid[x, y];

        player.Initialize(this, new Vector2Int(x, y));
    }

    private bool PlayerSafeZone(int x, int y)
    {
        return ((x == 1 || x == 2) && y == size - 2) || (x == 1 && y == size - 3);
    }
    #endregion

    #region Public Methods
    private readonly Vector2Int[] crossDirections = new Vector2Int[]
    {
        new Vector2Int(0,  1),
        new Vector2Int(0, -1),
        new Vector2Int( 1, 0),
        new Vector2Int(-1, 0),
    };
    public Vector2Int[] AvailableDirections(int x, int y)
    {
        List<Vector2Int> directions = new List<Vector2Int>();

        foreach (var direction in crossDirections)
        {
            int newX = x + direction.x;
            int newY = y + direction.y;

            // Matrix Bound check isn't necessary since the borders are all blocked by the blocks
            if (IsAvailable(newX, newY) || IsPlayer(newX, newY))
                directions.Add(direction);
        }

        return directions.ToArray();
    }

    public bool IsAvailable(int x, int y)
    {
        return grid[x, y].type == CellType.None;
    }


    public void SwapCell(Vector2Int first, Vector2Int second)
    {
        (grid[first.x, first.y], grid[second.x, second.y]) = (grid[second.x, second.y], grid[first.x, first.y]);
    }

    public void SetPlayerCell(int x, int y)
    {
        if (!IsAvailable(x, y)) throw new CellUnavailableException();

        grid[x, y] = playerCell;
    }

    public void PlaceBomb(int x, int y)
    {
        if (bombObject.activeInHierarchy) return;

        grid[x, y] = bombCell;

        bombObject.transform.position = new Vector3Int(x - size / 2, y - size / 2);
        bomb.gridPosition = new Vector2Int(x, y);
        
        bombObject.SetActive(true);
    }
    
    public bool IsBomb(int x, int y)
    {
        return grid[x, y].type == CellType.Bomb;
    }

    public bool IsPlayer(int x, int y)
    {
        return grid[x, y].type == CellType.Player;
    }

    public void Explode(Vector2Int origin, int radius)
    {
        DeleteCell(origin);

        if(player.GridPosition == origin)
        {
            GameOver();
        }
        //.Log(origin);

        for(int i = 0; i < crossDirections.Length; i++)
        {
            for (int j = 1; j <= radius; j++)
            {
                Vector2Int explosionPosition = origin + (crossDirections[i] * j);

                //Debug.Log(explosionPosition);

                Cell hittedCell = grid[explosionPosition.x, explosionPosition.y];

                bool hittedSomething = false;
                switch (hittedCell.type)
                {
                    case CellType.Player:
                        GameOver();
                        hittedSomething = true;
                        break;
                    case CellType.Enemy:
                        explodedEnemies++;
                        if (explodedEnemies == enemiesToSpawn)
                            GameWon();

                        DeleteCell(explosionPosition);
                        hittedSomething = true;
                        break;
                    case CellType.Brick:

                        DeleteCell(explosionPosition);
                        hittedSomething = true;
                        break;

                    case CellType.Block:
                        hittedSomething = true;
                        break;
                }

                if (hittedSomething)
                    break;
            }
        }
    }

    public void GameOver()
    {
        SceneManager.LoadScene("LoseScene");
    }

    public void GameWon()
    {
        SceneManager.LoadScene("WinScene");

    }
    #endregion
    private void DeleteCell(Vector2Int explosionPosition)
    {
        grid[explosionPosition.x, explosionPosition.y].gameObject.SetActive(false);
        grid[explosionPosition.x, explosionPosition.y] = emptyCell;
    }

}
