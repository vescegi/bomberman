using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] float turnSpeed;
    [SerializeField] float explosionTime;
    [SerializeField] int explosionRadius;
    [HideInInspector] public Vector2Int gridPosition;

    private Grid grid;

    public void Initialize(Grid grid)
    {
        this.grid = grid;
    }

    private void OnEnable()
    {
        StartCoroutine(Explode());
    }

    private IEnumerator Explode()
    {
        yield return new WaitForSeconds(explosionTime);

        grid.Explode(gridPosition, explosionRadius);
    }

    private void Update()
    {
        transform.Rotate(Vector3.forward * turnSpeed * Time.deltaTime);
    }
}
